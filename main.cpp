///  Tank Game
//   Tank Game is copywrite 2016 by the Fund Comp Losers who include James Marvin, Matt Flanagan, Matt Lanus, & Nicholas Carroll
// this game is available under the MIT license

//This game is Tank Game.  inspired by hit games such as combat for the atari VCS in 1977, and the tanks minigame from wii play  2007 for the nintendo wii, tank
//Using SDL, SDL_image, standard IO, and strings
#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>
#include <SDL2_ttf/SDL_ttf.h>
#include <SDL2_mixer/SDL_mixer.h>
#include <stdio.h>
#include <string>
#include <cmath>
#include <time.h>
#include <vector>
//#include "wall.h"

Mix_Chunk *pew = NULL;

Mix_Chunk *explode =NULL;

Mix_Chunk *pewpew = NULL;

Mix_Chunk *jonathoncena =NULL;

Mix_Chunk *bwa = NULL;

Mix_Chunk *oop = NULL;

Mix_Music *mun = NULL;

Mix_Music *alt = NULL;

Mix_Chunk *fire = NULL;



//The music that will be played
Mix_Music *gMusic = NULL;


//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;


long long int frame =0;

SDL_Rect EvilTankRECT;

SDL_Rect altRECT;


//Button constants
const int BUTTON_WIDTH = 50;
const int BUTTON_HEIGHT = 50;
const int TOTAL_BUTTONS = 1;

const int TANKSIZE = 50;



int b =59;
int g = 109;
int r = 61;
// 59, 109, 61,

enum LButtonSprite
{
    BUTTON_SPRITE_MOUSE_OUT = 0,
    BUTTON_SPRITE_MOUSE_OVER_MOTION = 1,
    BUTTON_SPRITE_MOUSE_DOWN = 2,
    BUTTON_SPRITE_MOUSE_UP = 3,
    BUTTON_SPRITE_TOTAL = 4
};


std::string score = "score: 0";
std::string enemyScoreString = "score: 0";

//Mouse button sprites
SDL_Rect gSpriteClips[ BUTTON_SPRITE_TOTAL ];



//The window renderer
SDL_Renderer* gRenderer = NULL;


SDL_Color textColor = { 0, 0, 0 };

/*
 //Button constants
 const int BUTTON_WIDTH = 150;
 const int BUTTON_HEIGHT = 100;
 const int TOTAL_BUTTONS = 1;
 */



//Globally used font
TTF_Font *gFont = NULL;


#include "LButton.h"

//Buttons objects
LButton gButtons[ TOTAL_BUTTONS ];

LTexture playerTank;

LTexture evilTank;

LTexture altTank;

LTexture blueTank;

LTexture twoTank;


LTexture enemyScore;

//Rendered texture
LTexture gTextTexture;

LTexture title;

int zeroMode;
int zeroFrame;
int intenseZeroMode;
int intenseZeroFrame;
int inwall =0;
int absoluteZeroMode;
int absoluteZeroFrame;
int twoFrame;

#include "mediaLoad.h"
#include "ai.h"
#include "Player.h"
#include "Bullet.h"
#include "Wall.h"

using namespace std;

int main( int argc, char* args[] )
{
    int xshift=0;
    int yshift =0;
    int oldx = 0;
    int oldy = 0;
    Bullet playerBullet(SCREEN_WIDTH+20,SCREEN_HEIGHT+20);
    Bullet evilBullet(SCREEN_WIDTH+10,SCREEN_HEIGHT+10);
    int shoot = 0;
    int enemy_shoot = 0;
    int pscore = 0, escore = 0;
    int pscore_delay = 0, escore_delay = 0;
    double o, a, theta, dtheta;
    srand(time(NULL));
    int level = 1;
    
    
    int two =0;
    
    player tank(25,25,0 , 1);
    player tank2(100, 100, 100, 2);
    //creates a vector of wall objects for the screens
    vector<Wall> walls(8);
       
    
    //Start up SDL and create window
    if( !init() )
    {
        printf( "Failed to initialize!\n" );
    }
    else
    {
        //Load media
        if( !loadMedia() )
        {
            printf( "Failed to load media!\n" );
        }
        else
        {
            //Main loop flag
            bool quit = false;
            
            //Event handler
            SDL_Event e;
            ai enemy(SCREEN_WIDTH - 25,SCREEN_HEIGHT - 25); //instantiation of the ai object and gives it an initial position
            int t[3];
            t[0]=0;
            t[1]=0;
            t[2]=0;
            //While application is running
            while( !quit )
            {
                
                if (level == 3) {
                    walls[0].setWalls(3*SCREEN_WIDTH/4, 3*SCREEN_HEIGHT/4-50, 20, 110);
                    walls[1].setWalls(1*SCREEN_WIDTH/4, 3*SCREEN_HEIGHT/4-50, 20, 110);
                    walls[2].setWalls(3*SCREEN_WIDTH/4, 1*SCREEN_HEIGHT/4-50, 20, 110);
                    walls[3].setWalls(1*SCREEN_WIDTH/4, 1*SCREEN_HEIGHT/4-50, 20, 110);
                    walls[4].setWalls(120,120, 110, 20);
                    walls[5].setWalls(440,120, 110, 20);
                    walls[6].setWalls(125,350, 110, 20);
                    walls[7].setWalls(440,350, 110, 20);
                }
                else if (level == 2) {
                    walls[0].setWalls(3*SCREEN_WIDTH/4, 2*SCREEN_HEIGHT/4-50, 20, 110);
                    walls[1].setWalls(1*SCREEN_WIDTH/4, 2*SCREEN_HEIGHT/4-50, 20, 110);
                    walls[2].setWalls(2*SCREEN_WIDTH/4, 1*SCREEN_HEIGHT/4-50, 20, 110);
                    walls[3].setWalls(2*SCREEN_WIDTH/4, 3*SCREEN_HEIGHT/4-50, 20, 110);
                    walls[4].setWalls(120,120, 110, 20);
                    walls[5].setWalls(440,120, 110, 20);
                    walls[6].setWalls(125,350, 110, 20);
                    walls[7].setWalls(440,350, 110, 20);
                }
                else if (level == 1) {
                    walls[0].setWalls(2*SCREEN_WIDTH/4-25, 2*SCREEN_HEIGHT/4-25, 50, 50);
                    walls[1].setWalls(2*SCREEN_WIDTH/4-25, 2*SCREEN_HEIGHT/4-25, 50, 50);
                    walls[2].setWalls(2*SCREEN_WIDTH/4-25, 2*SCREEN_HEIGHT/4-25, 50, 50);
                    walls[3].setWalls(2*SCREEN_WIDTH/4-25, 2*SCREEN_HEIGHT/4-25, 50, 50);
                    walls[4].setWalls(4*SCREEN_WIDTH/5, 2*SCREEN_HEIGHT/4-80, 20, 180);
                    walls[5].setWalls(1*SCREEN_WIDTH/5, 2*SCREEN_HEIGHT/4-80, 20, 180);
                    walls[6].setWalls(2*SCREEN_WIDTH/4-80, 1*SCREEN_HEIGHT/5, 180, 20);
                    walls[7].setWalls(2*SCREEN_WIDTH/4-80, 4*SCREEN_HEIGHT/5, 180, 20);
                }
                else {
                    walls[0].setWalls(3*SCREEN_WIDTH/4, 3*SCREEN_HEIGHT/4-50, 20, 110);
                    walls[1].setWalls(1*SCREEN_WIDTH/4, 3*SCREEN_HEIGHT/4-50, 20, 110);
                    walls[2].setWalls(3*SCREEN_WIDTH/4, 1*SCREEN_HEIGHT/4-50, 20, 110);
                    walls[3].setWalls(1*SCREEN_WIDTH/4, 1*SCREEN_HEIGHT/4-50, 20, 110);
                    walls[4].setWalls(120,120, 110, 20);
                    walls[5].setWalls(440,120, 110, 20);
                    walls[6].setWalls(125,350, 110, 20);
                    walls[7].setWalls(440,350, 110, 20);
                }

                
                //Handle events on queue
                while( SDL_PollEvent( &e ) != 0 )
                {
                    //User requests quit
                    if( e.type == SDL_QUIT )
                    {
                        quit = true;
                    }
                    else if( e.type == SDL_KEYDOWN )
                    {
                        //Select surfaces based on key press
                        switch( e.key.keysym.sym )
                        {
                                
                            case SDLK_SPACE: //space shoots bullets for the player and sets the shoot condition to 1
                                if (shoot == 0)
                                {
                                    Mix_PlayChannel( -1, pew, 0 );
                                    playerBullet.set(tank.getxcenter(), tank.getycenter(), tank.getAngle());
                                    shoot = 1;
                                }
                                break;
                            case SDLK_f:
                                if (two)
                                {
                                    if (enemy_shoot == 0)
                                    {
                                        Mix_PlayChannel( -1, pew, 0 );
                                        evilBullet.set(tank2.getxcenter(), tank2.getycenter(), tank2.getAngle());
                                        enemy_shoot = 1;
                                    }
                                    
                                }
                                break;
                            case SDLK_2:
                                if (two == 0){
                                    
                                    twoFrame =frame;
                                two = 1;
                                }
                                else{
                                    two = 0;
                                    twoFrame = 0;
                                }
                                break;
                            case SDLK_0:
                                if (intenseZeroMode == 1)
                                {
                                    absoluteZeroMode = 1;
                                    absoluteZeroFrame = frame;
                                    Mix_PauseMusic();
                                    gMusic = alt;
                                    Mix_PlayMusic(gMusic, -1);
                                    intenseZeroMode =  0;
                                    tank.setSpeed(30);
                                    playerTank = blueTank;
                                }
                                else if (zeroMode == 1)
                                {
                                    intenseZeroMode = 1;
                                    intenseZeroFrame = frame;
                                }
                                else{
                                zeroMode = 1;
                                pew = pewpew;
                                explode = bwa;
                                gSpriteClips[ 0 ] = altRECT;
                                Mix_PlayChannel( -1, pewpew, 0 );
                                zeroFrame = frame;
                                playerTank = altTank;
                                b = rand()%250;
                                g = rand()%250;
                                r = rand()%250;
                                }
                                break;
                                
                            case SDLK_1:
                                // Mix_PlayMusic( gMusic, -1 );
                                
                                if( Mix_PlayingMusic() == 0 )
                                {
                                    //Play the music
                                    Mix_PlayMusic( gMusic, -1 );
                                }
                                
                                //If music is being played
                                else
                                {
                                    //If the music is paused
                                    if( Mix_PausedMusic() == 1 )
                                    {
                                        //Resume the music
                                        Mix_ResumeMusic();
                                    }
                                    //If the music is playing
                                    else
                                    {
                                        //Pause the music
                                        Mix_PauseMusic();
                                    }
                                }
                                break;
                                
                            case SDLK_v:
                                Mix_PauseMusic();
                                Mix_PlayChannel( -1, jonathoncena, 0 );
                                Mix_PlayMusic(gMusic, -1);
                                break;
                                
                            case SDLK_m:
                                Mix_PauseMusic();
                                Mix_PlayChannel(-1, oop, 0);
                                Mix_PlayMusic(gMusic, -1);
                                break;
                            
                            case SDLK_n:
                                Mix_PauseMusic();
                                gMusic = mun;
                                Mix_PlayMusic(gMusic, -1);
                                break;
                                
                            case SDLK_l:
                                pew = fire;
                                break;
                                                                
                            default:
                                xshift+=0;
                                yshift+=0;
                                break;
                        }
                        
                        if (two != 1)
                        tank.move(e);
                        
                       
                    }
                }
                
                //Clear screen
                SDL_SetRenderDrawColor( gRenderer, b, g, r, 100 );
                SDL_RenderClear( gRenderer );
                
                if (frame < 120 ){
                     Mix_PlayChannel( -1, jonathoncena, 0 );
                    
                    title.render( ( SCREEN_WIDTH - gTextTexture.getWidth() ) / 2, (SCREEN_HEIGHT- gTextTexture.getHeight()) /2 );
                    title.loadFromRenderedText( "tank game", textColor );
                }
                else if (frame < 240 ){
                    
                    title.render( ( SCREEN_WIDTH - gTextTexture.getWidth() ) / 4, (SCREEN_HEIGHT- gTextTexture.getHeight()) /2 );
                    title.loadFromRenderedText( "by the fund comp losers, including", textColor );
                }
                else if (frame < 360 ){
                    
                    title.render( ( SCREEN_WIDTH - gTextTexture.getWidth() ) / 8, (SCREEN_HEIGHT- gTextTexture.getHeight()) /2 );
                    title.loadFromRenderedText( "matt lanus, james marvin, matt flanagan, & nick carroll", textColor );
                }
                else if (frame < 480 ){
                    
                    title.render( ( SCREEN_WIDTH - gTextTexture.getWidth() ) / 2, (SCREEN_HEIGHT- gTextTexture.getHeight()) /2 );
                    title.loadFromRenderedText( "enjoy", textColor );
                }
                else if ((intenseZeroMode == 0)&& (zeroMode == 1) && ((frame -zeroFrame)< 120)){
                    title.render( ( SCREEN_WIDTH - gTextTexture.getWidth() ) / 2, (SCREEN_HEIGHT- gTextTexture.getHeight()) /2 );
                    title.loadFromRenderedText( "zero mode activated", textColor );
                    
                }
                else if ((intenseZeroMode == 1) && ((frame -intenseZeroFrame)< 120)){
                    title.render( ( SCREEN_WIDTH - gTextTexture.getWidth() ) / 2, (SCREEN_HEIGHT- gTextTexture.getHeight()) /2 );
                    title.loadFromRenderedText( "intense zero mode activated", textColor );
                    
                }
                else if ((absoluteZeroMode == 1) && ((frame -absoluteZeroFrame)< 120)){
                    title.render( ( SCREEN_WIDTH - gTextTexture.getWidth() ) / 2, (SCREEN_HEIGHT- gTextTexture.getHeight()) /2 );
                    title.loadFromRenderedText( "absolute zero mode activated.  Gotta go fast", textColor );
                    
                }else if ((two) && ((frame -twoFrame)< 120)){
                    title.render( ( SCREEN_WIDTH - gTextTexture.getWidth() ) / 2, (SCREEN_HEIGHT- gTextTexture.getHeight()) /2 );
                    title.loadFromRenderedText( "two player mode activated", textColor );
                    
                }
                else{
                    
                if (intenseZeroMode ==1)
                {
                    b = rand()%250;
                    g = rand()%250;
                    r = rand()%250;
                     enemy.move(tank.getxcenter(),tank.getycenter());
                   
                }
                    if (two)
                    if (frame%5 == 1){
                    tank.movePress();
                    if (two)
                        tank2.movePressTwo();
                    }
                    if( Mix_PlayingMusic() == 0 )
                    {
                        //Play the music
                        Mix_PlayMusic( gMusic, -1 );
                    }
                //render ai object
                    if (!two)
                enemy.render();
                tank.render();
                    if (two)
                    tank2.renderTwo();
                //logic for determining when the ai should shoot
                o = tank.getycenter()-enemy.getycenter();
                a = tank.getxcenter()-enemy.getxcenter();
                if (a > 0)
                {
                    theta = atan(o/a)*180/M_PI;
                }
                else
                {
                    theta = atan(o/a)*180/M_PI + 180;
                }
                dtheta = theta - enemy.getAngle();
                if (!two)
                if (dtheta > -7 && dtheta < 7 && enemy_shoot == 0) //if the player is in sight and the shoot condition for enemy is zero shoot
                {
                    evilBullet.set(enemy.getxcenter(), enemy.getycenter(), enemy.getAngle());
                    enemy_shoot = 1;
                    
                }
                
                //gButtons[ 0 ].render(10);
                if (shoot == 1) { //moves and renders the player's bullet
                    playerBullet.moving();
                    playerBullet.render();
                }
                
                if (enemy_shoot == 1) { //moves and renders the ai's bullet
                    evilBullet.moving();
                    evilBullet.render();
                }
                
                
               
                
                //Render current frame
                gTextTexture.render( ( SCREEN_WIDTH - gTextTexture.getWidth() ) / 4, 10 );
                enemyScore.render( 3*( SCREEN_WIDTH - gTextTexture.getWidth() ) / 4, 10 );
                
                for (int i = 0; i < walls.size(); i++){ //checks the vector of walls for collision detection
                    walls[i].collision(&tank, &enemy, e, oldx, oldy);
                    walls[i].bCollision(&playerBullet, &evilBullet);
                    if (two)
                        walls[i].tankCollision(&tank2);
                    walls[i].render();
                }
                
                //checks if the player's bullet hit the enemy
                //if (!two)
                if ((playerBullet.hit(enemy.getxcenter(),enemy.getycenter()) && pscore_delay > 10) || (playerBullet.hit(tank2.getxcenter(),tank2.getycenter()) && (two) && pscore_delay > 10)){
                    pscore+=1;
                    score = "score: ";
                    cout << "Player scores and has "<<pscore<<" points"<< endl;
                    score += std::to_string(pscore);
                    pscore_delay = 0;
                    shoot = 0;
                    if (pscore >= 10) {
                        if (level < 3) {
                            level++;
                            pscore = 0;
                            escore = 0;
                            enemyScoreString = "score: ";
                            score = "score: ";
                            score += std::to_string(pscore);
                            enemyScoreString += to_string(escore);
                            tank.setPosition(25, 25);
                            enemy.setPosition(SCREEN_WIDTH-25, SCREEN_HEIGHT-25, 0);
                            continue;
                        }
                        else {
                            level = 1;
                            pscore = 0;
                            escore = 0;
                            enemyScoreString = "score: ";
                            score = "score: ";
                            score += std::to_string(pscore);
                            enemyScoreString += to_string(escore);
                            tank.setPosition(25, 25);
                            enemy.setPosition(SCREEN_WIDTH-25, SCREEN_HEIGHT-25, 0);
                            continue;
                        }

                    }
                    playerBullet.set(SCREEN_WIDTH+20, SCREEN_HEIGHT+20, 0);
                    int x = rand()%SCREEN_WIDTH;
                    int y = rand()%SCREEN_HEIGHT;
                    int ang = rand()%360;
                    if (!two)
                    enemy.setPosition(x, y, ang);
                    if (two)
                        tank2.setPosition(x, y);
                    inwall =0;
                    for (int i = 0; i < walls.size(); i++){ //checks the vector of walls for collision detection
                        inwall += walls[i].inside(x,y);
                        
                    }
                    while(inwall >0){
                        int wallHit=0;
                        for (int i = 0; i < walls.size(); i++){ //checks the vector of walls for collision detection
                            if (walls[i].inside(x,y))
                            {
                                x = rand()%SCREEN_WIDTH;
                                y = rand()%SCREEN_HEIGHT;
                                if (!two)
                                    enemy.setPosition(x, y, ang);
                                if (two)
                                    tank2.setPosition(x, y);
                                wallHit += 1;
                                break;
                                
                            }
                            
                            
                        }
                        if (wallHit == 0)
                            break;
                    }
                }
                //checks if the ai's bullet hit the player
                if(evilBullet.hit(tank.getxcenter(), tank.getycenter()) && escore_delay > 10) {
                    escore += 1;
                    enemyScoreString = "score: ";
                    cout << "Enemy scores and has "<<escore<<" points"<< endl;
                    enemyScoreString += to_string(escore);
                    escore_delay = 0;
                    enemy_shoot = 0;
                    if (escore >= 10) {
                        if (level < 3) {
                            level++;
                            pscore = 0;
                            escore = 0;
                            enemyScoreString = "score: ";
                            score = "score: ";
                            score += std::to_string(pscore);
                            enemyScoreString += to_string(escore);
                            tank.setPosition(25, 25);
                            enemy.setPosition(SCREEN_WIDTH-25, SCREEN_HEIGHT-25, 0);
                            continue;
                        }
                        else {
                            level = 1;
                            pscore = 0;
                            escore = 0;
                            enemyScoreString = "score: ";
                            score = "score: ";
                            score += std::to_string(pscore);
                            enemyScoreString += to_string(escore);
                            tank.setPosition(25, 25);
                            enemy.setPosition(SCREEN_WIDTH-25, SCREEN_HEIGHT-25, 0);
                            continue;
                        }
                    }
                    evilBullet.set(SCREEN_WIDTH+10, SCREEN_HEIGHT+10, 0);
                    
                   
                    
                    int x = rand()%SCREEN_WIDTH;
                    int y = rand()%SCREEN_HEIGHT;
                    
                    tank.setPosition(x,y);
                    
                    inwall =0;
                    for (int i = 0; i < walls.size(); i++){ //checks the vector of walls for collision detection
                        inwall += walls[i].inside(x,y);
                       
                    }
                    while(inwall >0){
                        int wallHit=0;
                        for (int i = 0; i < walls.size(); i++){ //checks the vector of walls for collision detection
                            if (walls[i].inside(x,y))
                            {
                                x = rand()%SCREEN_WIDTH;
                                y = rand()%SCREEN_HEIGHT;
                                tank.setPosition(x,y);
                                wallHit += 1;
                                break;
                                
                            }
                          
                            
                        }
                        if (wallHit == 0)
                            break;
                    }

                    
                }
                
                
                
                gTextTexture.loadFromRenderedText( score, textColor );
                enemyScore.loadFromRenderedText(enemyScoreString, textColor);
                escore_delay++;
                pscore_delay++;
                
                //moves the ai based on time
                if ((t[0]%5==0) && (two != 1)) {
                    oldx = enemy.getxcenter();
                    oldy = enemy.getycenter();
                    enemy.move(tank.getxcenter(),tank.getycenter());
                    t[0]++;
                }
                else
                {
                    t[0]++;
                }
                if (shoot == 1)
                    t[1]++;
                if (enemy_shoot == 1)
                    t[2]++;
                if (t[1]%200 == 0)  //waits or recharges bullets
                    shoot = 0;
                if (t[2]%200 == 0)
                    enemy_shoot = 0;
                }
                frame++;
                //Update screen
                    
                SDL_RenderPresent( gRenderer );
            }
        }
    }
    
    //Free resources and close SDL
    close();
    
    return 0;
}

