//
//  object.h
//  deliverable
//
//  Created by James Marvin on 4/22/16.
//  Copyright (c) 2016 James Marvin. All rights reserved.
//

#ifndef object_h
#define object_h
#include<string>
#include<iostream>
#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>
using namespace std;
class object { //parent class for ai, player, wall, and bullet
    
    
public:
    object(int , int); //takes in x and y coordinate
    ~object(); //deconstructor
    virtual void render()=0; //virtual render function that draws all of our objects
    
protected:
    int xcenter;
    int ycenter;
    
};

object::object(int x, int y){ //sets the x and y center attribute all objects have 
    xcenter=x;
    ycenter=y;
}
object::~object(){
    
}
void object::render(){ //simple implementation of render this is a placeholder
    cout << "This is a test" << endl;
}
#endif
