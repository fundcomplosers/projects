//
// ai.h
//  tankgame
//
//  Created by James Marvin
//  Copyright © 2016 JM. All rights reserved.
//

#ifndef _ai_h
#define _ai_h

#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>
#include <stdio.h>
#include <string>
#include <cmath>
#include <string>
#include "object.h"




const int screenwidth = 640;
const int screenheight = 480;
using namespace std;

class ai: public object { //inherits from object class
    friend class Wall; //creates the wall as friend so it can access position of walls
private:
    double angle; //direction for ai to move
    
public:
    ai(int , int ); //constructor takes in the x and y inherited from object and sets angle to zero
    void move(int , int); //takes in the coordinates of the player
    void setPosition(double, double, double);
    int getxcenter();
    int getycenter();
    double getAngle();
    
    int tooClose(int, int); //stops the ai from moving toward the player when it gets within a certain distance
    
    virtual void render(); //draws the enemy tank
    
    
    
};



ai::ai(int x, int y) : object(x, y){
    
    angle=0;
}

void ai::setPosition(double a, double b, double ang) {
    xcenter = a;
    ycenter = b;
    angle = ang;
}

int ai::tooClose(int x, int y){
    double dist;
    int xdist;
    int ydist;
    xdist=x-xcenter;    //calculating distance between ai and player
    ydist=y-ycenter;
    dist= pow(xdist, 2)+pow(ydist,2);
    double sqrdist;
    sqrdist = sqrt(dist);
    
    if (sqrdist<25){ //moves the ai back if player advances toward a stopped ai
        if (xcenter < x)
            xcenter-=10;
        else if (xcenter > x)
            xcenter+=10;
        else if (ycenter > y)
            ycenter+=10;
        else if (ycenter < y)
            ycenter-=10;
        return 1;
    }
    else if (sqrdist<50) {
        return 1;
    }
    else{
        return 0;
    }
}



void ai::move(int x, int y){
    
    
    double o = y-ycenter;
    double a = (x-xcenter); //calculating the angle between the ai and the player
    double theta;
    if (a > 0){
        theta = atan(o/a)*(180/M_PI);
    }
    else
    {
        theta = atan(o/a)*(180/M_PI) + 180;
    }
    
    int xChange =5*cos(angle * M_PI / 180.0 );
    int yChange =5*sin(angle * M_PI / 180.0);
    
    if (tooClose(x, y)) {
        xcenter-=xChange;
        ycenter-=yChange;
        //do nothing
    }
    else
    {
        double dtheta = theta -angle; //sees how many degrees the two tanks are off from eachother
    
       // int xChange =5*cos(angle * M_PI / 180.0 );
        //int yChange =5*sin(angle * M_PI / 180.0);
    
            if (dtheta > 7)
            {
                    angle += 5;
            }
            else if (dtheta < -7)       //if player is outside 14 degrees of tolerance rotate otherwise move forward
            {
                angle -= 5;
            }
            else{
                xcenter += xChange;
                ycenter += yChange;
            }
    }
    /// this part of the code allows the player to wrap around the map
    if (xcenter > SCREEN_WIDTH) // if the player is out of bounds to the right...
        xcenter -= SCREEN_WIDTH; // go to the left side of the screen
    else if (xcenter < 0) // if the player is out of bounds on the left side of the screen...
        xcenter += SCREEN_WIDTH; // go to the right
    if (ycenter > SCREEN_HEIGHT) // if greater than bottom
        ycenter -= SCREEN_HEIGHT; // go to top
    else if (ycenter < 0) // if above screen
        ycenter += SCREEN_HEIGHT; // go to the bottom...

}
int ai::getxcenter()
{
    return xcenter;
}
int ai::getycenter()
{
    return ycenter;
}

void ai::render()
{
    evilTank.render( getxcenter() - 25, getycenter() - 25, &EvilTankRECT, angle ); //draws the ai
}


double ai::getAngle()
{
    return angle;
}

#endif