//This program is the class description for the bullet class.
// This will cover the projectiles fired by the tanks
//Author: Matthew Flanagan

#ifndef BULLET_H
#define BULLET_H

#include <iostream>
#include "object.h"
#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>
#include <string>
#include <cmath>
using namespace std;


class Bullet : public object { //inherits from object class
    friend class Wall; //wall object is a friend so wall can access bullet info
    
public:
    Bullet(int, int);
    ~Bullet();
    bool hit(int, int); //registers if the bullet collides with a wall or tank
    void moving(); //moves the bullet across the screen
    int getX();
    int getY();
    void setX(int);
    void setY(int);
    void setXSpeed(double);
    void setYSpeed(double);
    double getXSpeed();
    double getYSpeed();
    void set(int x, int y, double angle); //sets the start position of the bullet
    virtual void render(); //draws the bullet
    
    
private:
    double xspeed;
    double yspeed;
    
    
};


Bullet::Bullet(int x, int y) : object(x, y) {
    
}

Bullet::~Bullet() {
    
}

int Bullet::getX() {
    return xcenter;
}

int Bullet::getY() {
    return ycenter;
}

double Bullet::getXSpeed() {
    return xspeed;
}

double Bullet::getYSpeed() {
    return yspeed;
}

void Bullet::setX(int val) { //sets the x
    xcenter = val;
}

void Bullet::setY(int val) { //sets the y
    ycenter = val;
}

void Bullet::setXSpeed(double val) { //sets speed in the x direction
    xspeed = val;
}

void Bullet::setYSpeed(double val) { //sets speed in the y direction
    yspeed = val;
}

void Bullet::set(int x, int y, double angle){ //takes in the start coordinates and the angle which it is fired
    
    
    double dx =5*cos(angle * M_PI / 180.0 );
    double dy =5*sin(angle * M_PI / 180.0);

    
    setYSpeed(dy);
    setXSpeed(dx);
    setX(x);
    setY(y);
}

void Bullet::moving() { //increments the position to create the illusion of the bullet moving across the screen 

    xcenter += xspeed;
    ycenter += yspeed;

}

bool Bullet::hit(int xtank, int ytank) {
    
    if ((xcenter < xtank + (TANKSIZE /2)) && ( xcenter > xtank - (TANKSIZE /2)))
        if ((ycenter < ytank + (TANKSIZE /2)) && (ycenter  > ytank - (TANKSIZE /2))) //recognizes if it is within the tank
        {
            Mix_PlayChannel( -1, explode, 0 );
            return 1;
        }
    

    
    return 0;
    
}

void Bullet::render(){ //implementation of the virtual render function
    
    SDL_Rect fillRect3 = { xcenter, ycenter, 10, 10};
    SDL_SetRenderDrawColor( gRenderer, 150, 150, 150, 150);
    SDL_RenderFillRect( gRenderer, &fillRect3);
}



#endif