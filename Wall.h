//
//  Wall.h
//  TankGame
//
//  Created by Nicholas Carroll on 4/25/16.
//  Copyright © 2016 Nicholas Carroll. All rights reserved.
//

// A simple wall class derived from object - creates a wall and able to detect if something has hit the wall

#ifndef Wall_h
#define Wall_h

#include "object.h"


class Wall: public object {
    
    friend class player; // player has access to the wall's private data
    
public:
    
    virtual void render(); // virtual function that displays the wall
    
    Wall(int x = SCREEN_WIDTH/2, int y = SCREEN_HEIGHT/2, int xlength = 30, int ylength = 130); //constructor with default values - global variables in the main program
    
    void collision(player *a, ai *b, SDL_Event e, double, double);  // checks for a tank collision
    void bCollision(Bullet *c, Bullet *d);// checks for a bullet collision
    void setWalls(double x, double y, int xlength, int ylength);        // change the position and size of wall - only used when creating an array of walls
    int inside(int x, int y);
    void tankCollision(player *a);
private:
    int xlen;
    int ylen;
    
};

// Constructor
Wall::Wall(int x, int y, int xlength, int ylength) : object(x,y){
    
    xlen=xlength;
    ylen=ylength;
}

// Changes the walls x and y coordinates and its width and height
void Wall::setWalls(double x, double y, int xlength, int ylength){
    xcenter = x;
    ycenter = y;
    xlen = xlength;
    ylen = ylength;
    
    
    
    
}


// Draws the wall as a rectangle
void Wall::render(){
    
    SDL_Rect fillRect3 = { xcenter, ycenter, xlen, ylen};
    SDL_SetRenderDrawColor( gRenderer, 150, 150, 150, 0);
    SDL_RenderFillRect( gRenderer, &fillRect3);
}


// checks for bullet collison with enemy bullet and player bullet
void Wall::bCollision(Bullet *c, Bullet *d){
    //xcenter and ycenter are not really x and y center, it is the top left corner (thanks SDL)
    int left = xcenter;
    int right = xcenter + xlen;
    int top = ycenter;
    int bottom = ycenter + ylen;

    // adds 5 to c->center (pointer and is actually the center of the bullet) to account for radius of bullet and checks if it is inside the wall
    if( (c->xcenter+5>left && c->xcenter-5<right) && (c->ycenter+5>top && c->ycenter-5<bottom)) {
        
        // if the bullet is "in" the wall, then display it off screen and set the speed to 0. Will reset once another bullet is shot
        
        //c->xcenter -= c->xspeed;
        //c->ycenter -= c->yspeed;
        //c->xspeed = -c->xspeed;
        if ((c->xcenter+5-c->xspeed<left)){
            
                 c->xcenter =  left-5;
                c->xspeed *=-1;
            }
        if ((c->xcenter+5-c->xspeed>right))
            {
                c->xcenter =  right+5;
                 c->xspeed *=-1;
            }
        if ((c->ycenter+5-c->yspeed<top)){
            
            c->ycenter =  top-5;
            c->yspeed *=-1;
        }
        if ((c->ycenter+5-c->yspeed>bottom))
        {
            c->ycenter =  bottom+5;
            c->yspeed *=-1;
        }
        
        
    }
    
    
    // same for the other bullet
    if( (d->xcenter+5>left && d->xcenter-5<right) && (d->ycenter+5>top && d->ycenter-5<bottom)){
        if ((d->xcenter+5-d->xspeed<left)){
            
            d->xcenter =  left-5;
            d->xspeed *=-1;
        }
        if ((d->xcenter+5-d->xspeed>right))
        {
            d->xcenter =  right+5;
            d->xspeed *=-1;
        }
        if ((d->ycenter+5-d->yspeed<top)){
            
            d->ycenter =  top-5;
            d->yspeed *=-1;
        }
        if ((d->ycenter+5-d->yspeed>bottom))
        {
            d->ycenter =  bottom+5;
            d->yspeed *=-1;
        }
        
    }
}

int Wall::inside(int x, int y)
{
    
    int left = xcenter;
    int right = xcenter + xlen;
    int top = ycenter;
    int bottom = ycenter + ylen;
    
    if ( (x+25>left && x-25<right) && (y+25>top && y-25<bottom))
    {
        cout << "in wall" << endl;
        return 1;
    }
    
    return 0;
}


void Wall::tankCollision(player *a)
{
    
    
    
    // same variables used in bcollision - refer to that method if don't understand variables
    int left = xcenter;
    int right = xcenter + xlen;
    int top = ycenter;
    int bottom = ycenter + ylen;
    
    // keeps the change made, so it can revert to it if the tank is inside the wall
    int xChange =5*cos(a->angle * M_PI / 180.0 );
    int yChange =5*sin(a->angle * M_PI / 180.0);
    
    // checks if tank is inside the wall (25 accounts for tank radius
    if ( (a->xcenter+25>left && a->xcenter-25<right) && (a->ycenter+25>top && a->ycenter-25<bottom))
    {
        /*
         switch( e.key.keysym.sym )
         {
         case SDLK_UP:
         a->ycenter -= yChange;
         a->xcenter -= xChange;
         break;
         
         case SDLK_DOWN:
         a->ycenter += yChange;
         a->xcenter += xChange;
         break;
         }
         */
        const Uint8* currentKeyStates = SDL_GetKeyboardState( NULL );
        
        if( currentKeyStates[ SDL_SCANCODE_W ] )
        {
            a->ycenter-=yChange;
            a->xcenter-=xChange;
            
            
        }
        else if( currentKeyStates[ SDL_SCANCODE_S ] )
        {
            a->ycenter+=yChange;
            a->xcenter+=xChange;
            
            
        }
        
    }
    
}

// Detects tank collision with wall
void Wall::collision(player *a, ai *b, SDL_Event e, double oldx, double oldy){
    
    
    // same variables used in bcollision - refer to that method if don't understand variables
    int left = xcenter;
    int right = xcenter + xlen;
    int top = ycenter;
    int bottom = ycenter + ylen;
    
    // keeps the change made, so it can revert to it if the tank is inside the wall
    int xChange =5*cos(a->angle * M_PI / 180.0 );
    int yChange =5*sin(a->angle * M_PI / 180.0);

    // checks if tank is inside the wall (25 accounts for tank radius
    if ( (a->xcenter+25>left && a->xcenter-25<right) && (a->ycenter+25>top && a->ycenter-25<bottom))
    {
        /*
        switch( e.key.keysym.sym )
        {
            case SDLK_UP:
                a->ycenter -= yChange;
                a->xcenter -= xChange;
                break;
                
            case SDLK_DOWN:
                a->ycenter += yChange;
                a->xcenter += xChange;
                break;
        }
         */
        const Uint8* currentKeyStates = SDL_GetKeyboardState( NULL );
        
        if( currentKeyStates[ SDL_SCANCODE_UP ] )
        {
            a->ycenter-=yChange;
            a->xcenter-=xChange;
            
            
        }
        else if( currentKeyStates[ SDL_SCANCODE_DOWN ] )
        {
            a->ycenter+=yChange;
            a->xcenter+=xChange;
            
            
        }
        
    }
    
    
    // Check if ai is inside the wall
    if ( (b->xcenter+25>left && b->xcenter-25<right) && (b->ycenter+25>top && b->ycenter-25<bottom))
    {

        b->xcenter = oldx;
        b->ycenter = oldy;
        
        xChange =5*cos(b->angle * M_PI / 180.0 );
        yChange =5*sin(b->angle * M_PI / 180.0);
        
        if ( (b->xcenter+25>xcenter && b->xcenter-25<xcenter+xlen) )
        {
            if (b->xcenter > a->xcenter)
                b->xcenter -=3;
            else
                b->xcenter +=3;
        }
        
        if ((b->ycenter+25>ycenter && b->ycenter-25<ycenter+ylen))
        {
            if (b->ycenter > a->ycenter)
                b->ycenter-=3;
            else
                b->ycenter +=3;
        }
        
        
        if ((b->xcenter > a->xcenter) && (b->ycenter > a->ycenter))
            if ((b->xcenter > left) && (b->ycenter < bottom))
                b->angle += 5;
            else
                b->angle -= 5;
        else if ((b->xcenter < a->xcenter) && (b->ycenter > a->ycenter))
             if ((b->xcenter < left) && (b->ycenter > bottom))
                b->angle -= 5;
             else
                b->angle += 5;
         else if ((b->xcenter > a->xcenter) && (b->ycenter < a->ycenter))
            if ((b->xcenter > left) && (b->ycenter > top))
                b->angle -= 5;
            else
                b->angle += 5;
         else if ((b->xcenter < a->xcenter) && (b->ycenter < a->ycenter))
            if ((b->xcenter < left) && (b->ycenter > top))
                b->angle += 5;
            else
                b->angle -= 5;
        
    }
    
    
    
}


#endif