//
//  player.h
//  deliverable
//
//  Created by James Marvin on 4/23/16.
//  Copyright (c) 2016 James Marvin. All rights reserved.
//


// The player is the main class for the user to control the tank - it is not controlled by ai movement - based on human movement

#ifndef player_h
#define player_h
#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>
#include <cmath>
#include <string>
#include "object.h"


using namespace std;
class player: public object {
    friend class Wall; // wall is friends with 
private:
    double angle;
    int speed;
    int num;
public:
    player(int , int, double, int num); //constructor
    void move(SDL_Event e);
    void movePress();
    void movePressTwo();
    void moveTwo(SDL_Event e);
    void setPosition(double, double);
    int getxcenter();
    int getycenter();
    double getAngle();
    virtual void render();
    void setSpeed(int velocity);
    void renderTwo();
   
    
};



player::player(int x, int y, double ang, int num) : object(x, y){
    angle=ang; // setting the extra data member for player
    speed =5;
    num = num;
    cout << num;
}

void player::setPosition(double a, double b) { // setting new position
    xcenter = a;
    ycenter = b;
}


void player::setSpeed(int velocity){
    speed = velocity;
}

// moves the player
void player::move(SDL_Event e ){
    
    // finds the advancement of the player
    int xChange =speed*cos(angle * M_PI / 180.0 );
    int yChange =speed*sin(angle * M_PI / 180.0);
    
    
    // Change position based on key press
    switch( e.key.keysym.sym )
    {
        case SDLK_UP: // if up key
           
            ycenter+=yChange;
            xcenter+=xChange;
           
            break;
            
        case SDLK_DOWN: // if down key
            ycenter-=yChange;
            xcenter-=xChange;
            break;
            
        case SDLK_LEFT: // if left
            angle-=speed; // change angle
            break;
    
            
        case SDLK_RIGHT: // if right key
            //xcenter+=10;
            angle+=speed;
            break;
       
            
        default:
            xcenter+=0;
            ycenter+=0;
            angle+=0;
            break;
    }
     /// this part of the code allows the player to wrap around the map
    if (xcenter > SCREEN_WIDTH) // if the player is out of bounds to the right...
        xcenter -= SCREEN_WIDTH; // go to the left side of the screen
    else if (xcenter < 0) // if the player is out of bounds on the left side of the screen...
        xcenter += SCREEN_WIDTH; // go to the right
    if (ycenter > SCREEN_HEIGHT) // if greater than bottom
        ycenter -= SCREEN_HEIGHT; // go to top
    else if (ycenter < 0) // if above screen
        ycenter += SCREEN_HEIGHT; // go to the bottom...
    
}
void player::movePress(){
    
    const Uint8* currentKeyStates = SDL_GetKeyboardState( NULL );
    
    int xChange =speed*cos(angle * M_PI / 180.0 );
    int yChange =speed*sin(angle * M_PI / 180.0);

    
    
   if( currentKeyStates[ SDL_SCANCODE_LEFT ] )
    {
        angle-=speed;
        
    }
    else if( currentKeyStates[ SDL_SCANCODE_RIGHT ] )
    {
        angle+=speed;
    }
    else if( currentKeyStates[ SDL_SCANCODE_UP ] )
    {
        ycenter+=yChange;
        xcenter+=xChange;
        
        
    }
    else if( currentKeyStates[ SDL_SCANCODE_DOWN ] )
    {
        ycenter-=yChange;
        xcenter-=xChange;
        
        
    }
    
    
    /// this part of the code allows the player to wrap around the map
    if (xcenter > SCREEN_WIDTH) // if the player is out of bounds to the right...
        xcenter -= SCREEN_WIDTH; // go to the left side of the screen
    else if (xcenter < 0) // if the player is out of bounds on the left side of the screen...
        xcenter += SCREEN_WIDTH; // go to the right
    if (ycenter > SCREEN_HEIGHT) // if greater than bottom
        ycenter -= SCREEN_HEIGHT; // go to top
    else if (ycenter < 0) // if above screen
        ycenter += SCREEN_HEIGHT; // go to the bottom...
    
}


void player::movePressTwo(){
    
    const Uint8* currentKeyStates = SDL_GetKeyboardState( NULL );
    
    int xChange =speed*cos(angle * M_PI / 180.0 );
    int yChange =speed*sin(angle * M_PI / 180.0);
    
    
    
    if( currentKeyStates[ SDL_SCANCODE_A ] )
    {
        angle-=speed;
        
    }
    else if( currentKeyStates[ SDL_SCANCODE_D ] )
    {
        angle+=speed;
    }
    else if( currentKeyStates[ SDL_SCANCODE_W ] )
    {
        ycenter+=yChange;
        xcenter+=xChange;
        
        
    }
    else if( currentKeyStates[ SDL_SCANCODE_S ] )
    {
        ycenter-=yChange;
        xcenter-=xChange;
        
        
    }
    
    
    /// this part of the code allows the player to wrap around the map
    if (xcenter > SCREEN_WIDTH) // if the player is out of bounds to the right...
        xcenter -= SCREEN_WIDTH; // go to the left side of the screen
    else if (xcenter < 0) // if the player is out of bounds on the left side of the screen...
        xcenter += SCREEN_WIDTH; // go to the right
    if (ycenter > SCREEN_HEIGHT) // if greater than bottom
        ycenter -= SCREEN_HEIGHT; // go to top
    else if (ycenter < 0) // if above screen
        ycenter += SCREEN_HEIGHT; // go to the bottom...
    
}

void player::moveTwo(SDL_Event e ){
    
    // finds the advancement of the player
    int xChange =speed*cos(angle * M_PI / 180.0 );
    int yChange =speed*sin(angle * M_PI / 180.0);
    
    
    // Change position based on key press
    switch( e.key.keysym.sym )
    {
        
            case SDLK_w: // if up key
                ycenter+=yChange;
                xcenter+=xChange;
                break;
                
            case SDLK_s: // if down key
                ycenter-=yChange;
                xcenter-=xChange;
                break;
                
            case SDLK_a: // if left
                angle-=speed; // change angle
                break;
                
            case SDLK_d: // if right key
                //xcenter+=10;
                angle+=speed;
                break;
        
        default:
            xcenter+=0;
            ycenter+=0;
            angle+=0;
            break;
    }
   
    /// this part of the code allows the player to wrap around the map
    if (xcenter > SCREEN_WIDTH) // if the player is out of bounds to the right...
        xcenter -= SCREEN_WIDTH; // go to the left side of the screen
    else if (xcenter < 0) // if the player is out of bounds on the left side of the screen...
        xcenter += SCREEN_WIDTH; // go to the right
    if (ycenter > SCREEN_HEIGHT) // if greater than bottom
        ycenter -= SCREEN_HEIGHT; // go to top
    else if (ycenter < 0) // if above screen
        ycenter += SCREEN_HEIGHT; // go to the bottom...
    
}


double player::getAngle() { // returns angle
    return angle;
}
int player::getxcenter() { // returns xcenter
    return xcenter;
}
int player::getycenter() { // returns ycenter
    return ycenter;
}


void player::render() { // "draws" the tank
    playerTank.render( getxcenter() -25 , getycenter() -25, &gSpriteClips[ 0 ], angle );
}


void player::renderTwo() { // "draws" the tank
    twoTank.render( getxcenter() -25 , getycenter() -25, &gSpriteClips[ 0 ], angle );
}


#endif
